---
layout: dienos
tags: [Bangkok, Thailand]
categories: [SEA]
diena: 4
dienu_sk: 0
nueita_snd: 20
nueita_viso: 86
temperatura: 34
nukeliauta: 10660
foto_sk: 36
komentarai_id: [3,7,10,22,28,29]
komentarai: [Vegetariski pusryciai, Orange linija, Wat Arun sventykla, Orchidejos geliu turguj, Wat Chakrawat sventykla, Bankoko meno ir kulturos centras]
formatai_id: [5,6,23,36]
formatai: [mp4,mov,mov,mov]
zemelapis: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2330225.999391625!2d99.03583322384269!3d13.57903995708254!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d6032280d61f3%3A0x10100b25de24820!2sBangkok!5e0!3m2!1sen!2sth!4v1579434361164!5m2!1sen!2sth" 
---
